<?php
include_once 'database.php';
include_once 'models.php';
$database = new Database();
$db = $database->getConnection();
$models = new Models($db);

$data = $models->view();
$html = "<table border=1>";
$html .= "<tr><td>Host</td><td>No of APi Calls</td></tr>";
while ($item = $data->fetch_array()) { 
	$html .= "<tr><td>".$item["host"]."</td>";
	$html .= "<td>".$item["calls"]."</td>";
	$html .= "</tr>";
}
$html .="</table>";

echo $html;

?>