<?php
class models{

    public $host;
    private $conn;
	
    public function __construct($db){
        $this->conn = $db;
    }	
		
   function create(){
		
	$stmt = $this->conn->prepare("
		INSERT INTO items (`host`,`created_at`)
		VALUES(?,?)");
	
	$this->host = htmlspecialchars(strip_tags($this->host));
	$created_at = date("Y-m-d H:i:s");
	
	
	$stmt->bind_param('ss',$this->host,$created_at);
	if($stmt->execute()){
		return true;
	}
 
	return false;		 
}

	function view(){
		
		$stmt = $this->conn->prepare("SELECT count(*) as calls,`host` from items group by `host`");
		
		$stmt->execute();
		$result = $stmt->get_result();		
		return $result;	
 
	}
}

?>